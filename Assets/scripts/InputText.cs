﻿using UnityEngine;
using UnityEngine.UI;

public class InputText : MonoBehaviour {

    public Text debugText;
    public GameObject bottomPanel, rightPanel, insertTextPanel, selectPanel;

    private DrawTextArea drawTAScript;
    public GameObject drawTextArea;
    TextMesh textMesh;
	private void Start(){
        drawTAScript = drawTextArea.GetComponent<DrawTextArea>();
        drawTAScript.enabled = false;
	}

	public void openKeyboard(){
        bottomPanel.gameObject.SetActive(false);
        rightPanel.gameObject.SetActive(false);
        selectPanel.gameObject.SetActive(false);

        drawTAScript.enabled = true;
        insertTextPanel.SetActive(true);
    }

    public void insertText(){
        togglePanels();
        if (drawTAScript.inputText != "")
        {
            debugText.text = drawTAScript.inputText;

            GameObject newText = Instantiate(Resources.Load("Text")) as GameObject;
            textMesh = newText.GetComponent<TextMesh>();
            textMesh.text = drawTAScript.inputText;

            Renderer textRenderer = newText.GetComponent<Renderer>();
            BoxCollider boxCollider = newText.AddComponent<BoxCollider>();
            boxCollider.size = new Vector3(GetWidth(textMesh), textRenderer.bounds.size.y, 1);
            newText.GetComponent<BoxCollider>().enabled = true;

            newText.GetComponent<Outline>().enabled = false;

            newText.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 1f;
            newText.transform.forward = Camera.main.transform.forward;
            newText.transform.localScale = new Vector3(0.01f, 0.01f, 0.1f);



            //Send text to server
            SessionObject newTextSessionObj = new SessionObjectText(textMesh.text,
                                                                 newText.transform.position,
                                                                 newText.transform.localScale,
                                                                 newText.transform.rotation);
            newText.name = newTextSessionObj.Uid.ToString();
            Session.Instance.AddObject(newTextSessionObj);
        }
        else
            Debug.Log("empty string!");
    }

    public void cancelInsert(){
        drawTAScript.inputText = "";
        togglePanels();
    }

    private void togglePanels(){
        drawTAScript.enabled = false;
        bottomPanel.gameObject.SetActive(true);
        selectPanel.gameObject.SetActive(true);
        insertTextPanel.SetActive(false);
    }

    public static float GetWidth(TextMesh mesh)
    {
        float width = 0;
        foreach (char symbol in mesh.text)
        {
            CharacterInfo info;
            if (mesh.font.GetCharacterInfo(symbol, out info, mesh.fontSize, mesh.fontStyle))
            {
                width += info.advance;
            }
        }
        return width * mesh.characterSize * 0.1f;
    }

    /*public static float getHeigth(TextMesh mesh)
    {
        int maxHeight = -1;
        float height = 0;
        foreach (char symbol in mesh.text)
        {
            CharacterInfo info;
            if (mesh.font.GetCharacterInfo(symbol, out info, mesh.fontSize, mesh.fontStyle))
            {
                if (maxHeight < info.glyphHeight)
                    maxHeight = info.glyphHeight;
            }
        }
        return maxHeight;
    }*/
}
