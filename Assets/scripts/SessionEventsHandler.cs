﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SessionEventsHandler : MonoBehaviour
{
    static GameObject targetObject;

    public static void eventOnConnectDone(object sender, Session.ConnectResult result)
    {
        switch (result) {
            case Session.ConnectResult.Success:
                Debug.Log("Connected");
                break;
            case Session.ConnectResult.Error:
            case Session.ConnectResult.SynchronizationError:
                Debug.Log("Connection failed, re-trying");
                Session.Instance.Connect(true);
                break;
            default:
                break;
        }
    }

    public static void eventOnDisconnected(object sender, Session.DisconnectReason reason)
    {
        if (reason == Session.DisconnectReason.ClosedByServer) {
            Debug.Log("Disconnected, reconnecting...");
            Session.Instance.Connect(true);
        }
    }

    public static void eventOnObjectSelectionChanged(object sender, SessionObjectSelectionChangedArgs args)
    {
        Debug.Log("OBJECT SELECTED EVENT");
        SessionObject obj = args.Obj;

        bool isRemoteObjSelected = args.IsSelected;
        targetObject = GameObject.Find(obj.Uid.ToString());


        Debug.Log("SELECTED: " + isRemoteObjSelected + "   OBJECT: " + obj.Uid);

        switch(obj.ObjectType){
            case "File":
                if (isRemoteObjSelected)
                {
                    targetObject.GetComponent<Outline>().enabled = true;
                    targetObject.GetComponent<Outline>().OutlineColor = Color.red;
                }
                else
                {
                    targetObject.GetComponent<Outline>().OutlineColor = Color.green;
                    targetObject.GetComponent<Outline>().enabled = false;
                }
                break;
            case "Text":
                if (isRemoteObjSelected)
                {
                    targetObject.GetComponent<Outline>().OutlineColor = Color.red;
                    targetObject.GetComponent<TextMesh>().color = Color.red;
                }
                else
                {
                    targetObject.GetComponent<Outline>().OutlineColor = Color.green;
                    targetObject.GetComponent<TextMesh>().color = Color.white;
                }
                break;
        }
    }

    public static void eventOnObjectAdded(object sender, SessionObject obj)
    {
        Debug.Log("ADDING NEW REMOTE OBJECT: " + obj.Uid);

        if(obj is SessionObjectFile)
        {
            //add new image in scene
            var sessionObjectFile = obj as SessionObjectRemoteFile;

            byte[] fileContent = sessionObjectFile.FileContent;
            var texture = new Texture2D(2, 2);
            texture.LoadImage(fileContent);

            GameObject newImage = Instantiate(Resources.Load("Cube")) as GameObject;
            newImage.GetComponent<Outline>().enabled = false;

            newImage.transform.position = sessionObjectFile.Position;
            newImage.transform.rotation = sessionObjectFile.Rotation;
            newImage.transform.localScale = sessionObjectFile.Scale;

            Material material = newImage.GetComponent<Renderer>().material;
            if (!material.shader.isSupported) // happens when Standard shader is not included in the build
                material.shader = Shader.Find("Legacy Shaders/Diffuse");

            material.mainTexture = texture;

            newImage.name = sessionObjectFile.Uid.ToString();
        } 
        else if (obj is SessionObjectLink) 
        {
            // TODO
        } 
        else if (obj is SessionObjectText) 
        {
            //add text in scene
            var sessionObjectText = obj as SessionObjectText;

            GameObject newText = Instantiate(Resources.Load("Text")) as GameObject;

            TextMesh textMesh = newText.GetComponent<TextMesh>();
            textMesh.text = sessionObjectText.Text;

            newText.GetComponent<Outline>().enabled = false;

            newText.transform.position = sessionObjectText.Position;
            newText.transform.localScale = sessionObjectText.Scale;
            newText.transform.rotation = sessionObjectText.Rotation;
            newText.transform.forward = Camera.main.transform.forward;

            Renderer textRenderer = newText.GetComponent<Renderer>();
            BoxCollider boxCollider = newText.AddComponent<BoxCollider>();
            boxCollider.size = new Vector3(GetWidth(textMesh), textRenderer.bounds.size.y, 1);
            newText.AddComponent<BoxCollider>().enabled = true;

            newText.name = sessionObjectText.Uid.ToString();
        }
        Debug.Log("EVENT Object added: " + obj.Uid.ToString());
    }

    public static void eventOnObjectMoved(object sender, SessionObject obj)
    {
        targetObject = GameObject.Find(obj.Uid.ToString());

        targetObject.transform.position = obj.Position;
        targetObject.transform.localScale = obj.Scale;
        targetObject.transform.rotation = obj.Rotation;
    }

    public static void eventOnObjectRemoved(object sender, SessionObject obj)
    {
        Debug.Log("EVENT Object removed: " + obj.Uid.ToString());
        targetObject = GameObject.Find(obj.Uid.ToString());
        if(targetObject != null)
            Destroy(targetObject);
    }

    //CALLED basic info about object are registered in the server --> not the image: could be used to show some thumnail
    public static void eventOnObjectAddedMetaDataOnly(object sender, SessionObject obj)
    {
        Console.WriteLine("EVENT Object added meta only: " + obj.Uid.ToString());
    }

    public static float GetWidth(TextMesh mesh)
    {
        float width = 0;
        foreach (char symbol in mesh.text)
        {
            CharacterInfo info;
            if (mesh.font.GetCharacterInfo(symbol, out info, mesh.fontSize, mesh.fontStyle))
            {
                width += info.advance;
            }
        }
        return width * mesh.characterSize * 0.1f;
    }

}
