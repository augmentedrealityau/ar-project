﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Threading;

public class Interactions : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;

    GameObject selectedObject;
    SessionObject selectedSessionObj;

    public GameObject ARCameraManager;

    Vector3 initialPosition, initialScale, screenCenter;
    Quaternion initialRotation;
    float distance, initialDistance;

    bool isSelected, isHitting;

    private float rotationValue;

    public GameObject rightPanel, bottomPanel, inputTextPanel, startCanvas, mainCanvas;
    public Slider scaleSlider, distanceSlider, rotateSlider;

    public Text debugText, debugText2;

    public Camera arCamera;

    private Session session;

    void Start()
    {
        isSelected = false;
        isHitting = false;
        rotationValue = 0;

        screenCenter = new Vector3(Screen.width / 2, Screen.height / 2, 0);

        ray = new Ray();
        rightPanel.SetActive(false);
        inputTextPanel.SetActive(false);
    }

    void Update()
    {

        ray = Camera.main.ScreenPointToRay(screenCenter);

        if (Physics.Raycast(ray, out hit))
        {
            isHitting = true;
            if (!isSelected )
            {
                selectedObject = hit.transform.gameObject;
                selectedSessionObj = session.GetCachedObject(new Guid(selectedObject.name));

                //update outline and text color
                if (!(selectedObject.GetComponent<Outline>().OutlineColor == Color.red)){
                    
                    if (selectedObject.GetComponent<TextMesh>() == null)
                        selectedObject.GetComponent<Outline>().enabled = true;
                    else
                        selectedObject.GetComponent<TextMesh>().color = Color.green;

                    selectedObject.GetComponent<Outline>().OutlineColor = Color.green;
                }

                distance = Vector3.Distance(Camera.main.transform.position, selectedObject.transform.position);
                initialDistance = distance;

                initialPosition = selectedObject.transform.position;
                initialScale = selectedObject.transform.localScale;
                initialRotation = selectedObject.transform.rotation;

            }
        }
        else
        {
            isHitting = false;
            if (selectedObject != null && !(selectedObject.GetComponent<Outline>().OutlineColor == Color.red))
            {
                selectedObject.GetComponent<Outline>().enabled = false;

                if (selectedObject.GetComponent<TextMesh>() != null)
                    selectedObject.GetComponent<TextMesh>().color = Color.white;   
            }
        }

        if (isSelected)
        {
            selectedObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, distance));

            if(selectedObject.GetComponent<TextMesh>() != null)
                selectedObject.transform.forward = Camera.main.transform.forward;
            else
                selectedObject.transform.forward = -Camera.main.transform.forward;
            
            selectedObject.transform.rotation = selectedObject.transform.rotation * Quaternion.Euler(Vector3.up * rotationValue);

            session.MoveObject(session.GetCachedObject(new Guid(selectedObject.name)), position: selectedObject.transform.position, rotation: selectedObject.transform.rotation);

            debugText.text = "POSITON CHANGED CALL: " + selectedObject.transform.position.ToString();
        }

        debugText2.text = "IS HITTING: " + isHitting;
    }


    //UI BUTTONS LISTERNERS


    public void startSession(){

        //UnityARSessionNativeInterface.GetARSessionNativeInterface().SetWorldOrigin(Camera.main.transform);

        Debug.Log("DEBUG: Starting AR Session");
        ARCameraManager.GetComponent<UnityARCameraManager>().enabled = true;

        //Set remote events handlers
        session = Session.Instance;
        session.Context = SynchronizationContext.Current;
        session.OnConnectDone += SessionEventsHandler.eventOnConnectDone;
        session.OnDisconnected += SessionEventsHandler.eventOnDisconnected;
        session.OnObjectSelectionChanged += SessionEventsHandler.eventOnObjectSelectionChanged;
        session.OnObjectAdded += SessionEventsHandler.eventOnObjectAdded;
        session.OnObjectMoved += SessionEventsHandler.eventOnObjectMoved;
        session.OnObjectRemoved += SessionEventsHandler.eventOnObjectRemoved;
        session.OnObjectAddedMetaDataOnly += SessionEventsHandler.eventOnObjectAddedMetaDataOnly;

        debugText2.text = "Start() -> Connecting";
        Debug.Log("DEBUG: Connect call");
        session.Connect();


        startCanvas.SetActive(false);
        mainCanvas.SetActive(true);

        debugText.text = "AR Session started";
        debugText2.text = "session.Connect()";
    }

    public void toggleSelect(){
        if (isHitting)
        {
            if (isSelected)
            {
                isSelected = false;
                selectedObject.GetComponent<Outline>().enabled = false;

                if (selectedObject.GetComponent<TextMesh>() != null)
                    selectedObject.GetComponent<TextMesh>().color = Color.white;

                rightPanel.SetActive(false);
                bottomPanel.SetActive(true);


                //notify server object is not selected anymore
                session.MarkObjectSelected(selectedSessionObj, isSelected);
            }
            else if(selectedObject.GetComponent<Outline>().OutlineColor != Color.red)
            {
                isSelected = true;

                if (selectedObject.GetComponent<TextMesh>() == null)
                    selectedObject.GetComponent<Outline>().enabled = true;
                else
                    selectedObject.GetComponent<TextMesh>().color = Color.blue;

                selectedObject.GetComponent<Outline>().OutlineColor = Color.blue;

                scaleSlider.value = 1.5f;
                distanceSlider.value = 0f;
                rotateSlider.value = 0;

                rightPanel.SetActive(true);
                bottomPanel.SetActive(false);

                //notify server this object is already selected
                session.MarkObjectSelected(selectedSessionObj, isSelected);
            }
            debugText.text = "Toggled to: "+isSelected + " isHitting: "+isHitting;
        }
    }

    public void updateScale(Slider slider){
        if(isSelected){
            float value = slider.value;
            selectedObject.transform.localScale = initialScale * value;

            session.MoveObject(session.GetCachedObject(new Guid(selectedObject.name)), scale : selectedObject.transform.localScale);
        }
    }

    public void updateDistance(Slider slider){
        if (isSelected)
        {
            distance = initialDistance + initialDistance * slider.value;
            selectedObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, distance));

            session.MoveObject(session.GetCachedObject(new Guid(selectedObject.name)), position: selectedObject.transform.position);
        }
    }

    public void updateRotation(Slider slider){
        if (isSelected){
            rotationValue = slider.value;
        }
    }


    public void deleteObject(){
        if(isSelected){
            Destroy(selectedObject);
            toggleSelect();

            session.RemoveObject(session.GetCachedObject(new Guid(selectedObject.name)));
        }
    }

    public void clearSession(){
        
        var sessionObjects = session.GetAllCachedObjects();
        foreach (var obj in sessionObjects)
            Destroy(GameObject.Find(obj.Uid.ToString()));
        
        session.RemoveAllObjects();
        debugText.text = "Deleted all objects";
    }
}
    