﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReadImage : MonoBehaviour {
   
    public Text text;
    public void readImage(){
        text.text = "Reading image";

        int maxSize = 1024;//input image max size in px
        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {
            Debug.Log("Image path: " + path);
            if (path != null)
            {
                // create texture from image 
                Texture2D texture = NativeGallery.LoadImageAtPath(path, maxSize);
                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    return;
                }

                // create new cube, in fron of camera and apply input image as texture
               // GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);

                GameObject newImage = Instantiate(Resources.Load("Cube")) as GameObject;
                newImage.GetComponent<Outline>().enabled = false;

                newImage.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 1f;
                newImage.transform.forward = -Camera.main.transform.forward;
                newImage.transform.localScale = new Vector3(1f, texture.height / (float)texture.width, 0.1f);

                Material material = newImage.GetComponent<Renderer>().material;
                if (!material.shader.isSupported) // happens when Standard shader is not included in the build
                    material.shader = Shader.Find("Legacy Shaders/Diffuse");

                material.mainTexture = texture;

                //Send object data to server
                SessionObject newImageSessionObj = new SessionObjectFile(path,
                                                                     newImage.transform.position,
                                                                     newImage.transform.localScale,
                                                                     newImage.transform.rotation);
                newImage.name = newImageSessionObj.Uid.ToString();
                Session.Instance.AddObject(newImageSessionObj);
            }
        }, "Select a PNG image", "image/png", maxSize);

        Debug.Log("Permission result: " + permission);
    }
}
