﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawTextArea : MonoBehaviour {

    public string inputText = "";
    GUIStyle textAreaStyle, rectStyle;
    Texture2D backTexture;
	private void Start()
	{
        backTexture = new Texture2D(1, 1);
        backTexture.SetPixel(0, 0, new Color(1f, 1f, 1f, 0.9f));
        backTexture.Apply();
	}

	private void OnGUI(){
        textAreaStyle = new GUIStyle(GUI.skin.textArea);
        textAreaStyle.fontSize = 40;

        textAreaStyle.normal.background = backTexture;
        textAreaStyle.hover.background = backTexture;

        rectStyle = new GUIStyle(GUI.skin.box);
        rectStyle.normal.background = backTexture;

        inputText = GUI.TextArea(new Rect(0, 0, Screen.width, 100), inputText, textAreaStyle);
	}
}
